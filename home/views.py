from django.shortcuts import render, redirect
from django.http import HttpResponse
from . import forms, models

# Create your views here.

def home(request):
    return render(request, 'index.html', {'name':'Praya'})

def music(request):
    return render(request, 'music.html')

def schedule(request):
    scheduleinput = {
        'scheduleform' : forms.ScheduleForm(),
        'list_of_schedules' : models.Schedule.objects.order_by('Date_and_time')
    }

    if request.method == "POST":
        form = forms.ScheduleForm(request.POST)
        if 'id' in request.POST:
            id = request.POST['id']
            models.Schedule.objects.get(id=id).delete()
            return redirect('schedule')

        elif form.is_valid():
            form.save()
            return redirect('schedule')

    return render(request, 'schedule.html', scheduleinput)
