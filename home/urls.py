from django.urls import include, path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('music', views.music, name='music'),
    path('schedule', views.schedule, name ='schedule'),
]

