from django.db import models
# Create your models here.
class Schedule(models.Model):
    Name_Of_Activity = models.CharField(max_length=27)
    Date_and_time = models.DateTimeField()
    Category = models.CharField(max_length=27)
    Place = models.CharField(max_length=27)
    
    def __str__(self):
        return self.Name_Of_Activity
# class ScheduleForm(modelForm):
#     class Meta:
#         model = Schedule
#         fields = ['nama_aktivitas', 'tempat_aktivitas', 'waktu_aktivitas', 'tanggal_aktivitas']