from django import forms
from .models import Schedule

class ScheduleForm(forms.ModelForm):
    Name_Of_Activity = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control', 'required' : True,}))
    Date_and_time = forms.DateTimeField(input_formats=['%Y/%m/%d %H:%M'], widget=forms.DateTimeInput(attrs={
        'class' : 'form-control', 'id' : 'datetimepicker', 'required' : True,}))
    # Time = forms.TimeField(requib red=True, widget=forms.TimeInput(attrs={
    #     'class' : 'form-control'}))
    # Date = forms.DateField(required=True, widget=forms.DateInput(attrs={
    #     'class' : 'form-control', 'id' : 'datepicker'}))
    Category = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control', 'required' : True,}))
    Place = forms.CharField(widget=forms.TextInput(attrs={
         'class' : 'form-control', 'required' : True,}))
    
    class Meta:
        model = Schedule
        fields = ['Name_Of_Activity', 'Date_and_time', 'Category', 'Place']